import { Route, Routes } from "react-router-dom";

import { GlobalDebug } from "./helpers/Common";
import HomePage from "./pages/HomePage";
import Layout from "./components/templates/Layout";
import LoginPage from "./pages/LoginPage";
import React from "react";
import RequireAuth from "./components/RequireAuth";

function App() {
	React.useEffect(() => {
		GlobalDebug(false);
	}, []);

	return (
		<div>
			<Layout>
				<Routes>
					<Route path="/" element={<LoginPage />} />
					<Route element={<RequireAuth />}>
						<Route path="/home" element={<HomePage />} />
					</Route>
				</Routes>
			</Layout>
		</div>
	);
}

export default App;
