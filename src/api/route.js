import { IoChatboxEllipsesSharp, IoHomeSharp } from "react-icons/io5";

const routes = [
	{
		title: "Home",
		link: "/home",
		haveChild: false,
		icon: <IoHomeSharp fill="#FFFFFF" />,
	},
	{
		title: "SMS",
		link: "/sms",
		haveChild: true,
		icon: <IoChatboxEllipsesSharp fill="#FFFFFF" />,
		childs: [
			{
				title: "Broadcast",
				link: "/broadcast",
				haveChild: false,
			},
			{
				title: "SMS Scheduller",
				link: "/scheduller",
				haveChild: false,
			},
		],
	},
];

export default routes;
