import { configureStore } from "@reduxjs/toolkit";
import navbarReducers from "../features/navbarSlice";

export const store = configureStore({
	reducer: {
		navbar: navbarReducers,
	},
	middleware: (getDefaultMiddleware) =>
		getDefaultMiddleware({
			serializableCheck: {
				// Ignore these action types
				ignoredActions: ["your/action/type"],
				// Ignore these field paths in all actions
				ignoredActionPaths: ["meta.arg", "payload.timestamp"],
				// Ignore these paths in the state
				ignoredPaths: ["items.dates"],
			},
		}),
});
