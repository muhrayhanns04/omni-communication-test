import { Navigate, Outlet, useLocation } from "react-router-dom";

function RequireAuth({ auth = true }) {
	const token = localStorage.getItem("@token");
	let location = useLocation();

	if (!token) {
		return <Navigate to="/" state={{ from: location }} replace />;
	}

	return <Outlet />;
}

export default RequireAuth;
