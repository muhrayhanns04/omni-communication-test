import { BeatLoader } from "react-spinners";
import React from "react";

const ButtonComponent = ({
	value,
	className,
	textClass,
	linkUrl,
	isLoading,
}) => {
	return (
		<button
			type="submit"
			target="_blank"
			className={`no-decoration flex w-full cursor-pointer flex-row items-center justify-center rounded-8 bg-red-500 py-16 no-underline hover:bg-red-600 ${className}`}
		>
			{isLoading ? (
				<BeatLoader color="#FFFFFF" size={16} />
			) : (
				<p
					className={`text-center font-semi-bold text-16 ${textClass}
 no-decoration text-white no-underline`}
				>
					{value}
				</p>
			)}
		</button>
	);
};

export default ButtonComponent;
