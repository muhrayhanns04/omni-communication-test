import { IoSearch } from "react-icons/io5";
import React from "react";

const InputHook = ({
	label,
	required = true,
	name,
	type,
	register,
	className: handleClass,
	autocomplete = "off",
	placeholder,
	inputClassName,
}) => {
	/**
		One of the more complicated component ways, but because of the basics we are use Tailwind CSS, it's better to just use className props than this config
		------------------------------------------------
		const customClass = {
		background: config?.background
			? config?.background
			: config?.background && !bordered
			? "bg-white"
			: "bg-gray-100",
		border: config?.border ? config?.border : "",
		padding: config?.padding ? config?.padding : "p-16",
		fontSize: config?.fontSize ? config?.fontSize : "text-16",
		paddinLeft: config?.paddingLeft
			? config?.paddingLeft
			: type === "search"
			? "pl-42"
			: "",
		};
 	*/

	return (
		<div className={`mb-16 ${handleClass} relative`}>
			<p className="mb-13 font-semi-bold text-16 text-gray-900">
				{required ? <span className="text-red-500">* </span> : null}
				{label}
			</p>

			<div className="relative flex w-full flex-row">
				<input
					{...register(name, { required })}
					placeholder={placeholder}
					type={type ? type : type === "search" ? "text" : "text"}
					autoComplete={autocomplete}
					className={`relative w-full rounded-8  placeholder-gray-400 ${inputClassName} font-regular text-gray-900`}
				/>
				{type === "search" && (
					<IoSearch className="absolute top-[35%] left-16" />
				)}
			</div>
		</div>
	);
};

export default InputHook;
