import ItemLayout from "../../organisms/Sidebar/ItemLayout";
import LOGO from "../../../assets/icons/logo-white.svg";
import React from "react";
import { getIsActive } from "../../../features/navbarSlice";
import routes from "../../../api/route";
import { useLocation } from "react-router-dom";
import { useSelector } from "react-redux";

const Sidebar = () => {
	const location = useLocation();
	const isActive = useSelector(getIsActive);

	if (location.pathname !== "/") {
		return (
			<section
				className={`fixed top-[92px]  ${
					isActive
						? "top-[80px] opacity-100 "
						: "top-0 opacity-0 lg:opacity-100"
				} z-20 h-[100vh] w-[210px] flex-col items-center bg-red-500 py-32 lg:sticky lg:top-0 lg:flex`}
			>
				<img
					src={LOGO}
					className="mb-32 hidden w-[80%] lg:block"
					alt="PT Omni Communication Assistance"
				/>

				<section className="relative flex w-full flex-col space-y-24 px-16">
					{routes?.map((item, index) => {
						return (
							<React.Fragment key={index}>
								<ItemLayout item={item} />
							</React.Fragment>
						);
					})}
				</section>
			</section>
		);
	} else {
		return <></>;
	}
};

export default Sidebar;
