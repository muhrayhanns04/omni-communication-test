import InputHook from "../../atoms/InputHook";
import React from "react";
import { TbAlignLeft } from "react-icons/tb";
import { toggleButton } from "../../../features/navbarSlice";
import { useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
import { useLocation } from "react-router-dom";

const Header = () => {
	const { register, handleSubmit } = useForm();
	const location = useLocation();
	const dispatch = useDispatch();

	let imageUrl =
		"https://images.unsplash.com/photo-1606122017369-d782bbb78f32?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8Z2lybHMlMjBwaG90b3xlbnwwfHwwfHw%3D&w=1000&q=80";

	const onSubmit = (data) => console.info("Submitted!");

	if (location.pathname !== "/") {
		return (
			<div className="sticky top-0 z-10 flex w-full flex-row items-center justify-between bg-white/30 py-8 px-24 backdrop-blur-sm lg:py-0">
				<section className="flex flex-row items-center">
					<button
						onClick={() => dispatch(toggleButton())}
						style={{
							WebkitTapHighlightColor: "transparent",
						}}
						className="mr-16 block rounded-8 border-none bg-gray-50 bg-no-repeat p-8 outline-none outline outline-offset-2 outline-red-500 lg:hidden"
					>
						<TbAlignLeft className="h-24 w-24" />
					</button>
					<form onSubmit={handleSubmit(onSubmit)}>
						<InputHook
							register={register}
							name="search"
							type="search"
							autocomplete="on"
							required={false}
							placeholder="Any help?"
							inputClassName="border-2 border-gray-100 p-13 pl-42 text-13 outline-none"
						/>
					</form>
				</section>
				<section className="flex flex-row items-center">
					<article className="mr-16 hidden flex-col items-end lg:flex">
						<h3 className=" text-16 font-bold text-black">
							Hi, Adjie!
						</h3>
						<p className="font-regular text-13 text-gray-300">
							Adjie_g4ant3ng@banget.com
						</p>
					</article>
					<figure>
						<img
							className="h-[36px] w-[36px] rounded-full outline outline-offset-2 outline-red-500"
							src={imageUrl}
							alt="profile - Adjie"
						/>
					</figure>
				</section>
			</div>
		);
	} else {
		return <></>;
	}
};

export default Header;
