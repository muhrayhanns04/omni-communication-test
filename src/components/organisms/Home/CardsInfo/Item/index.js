import React from "react";

const CardItem = ({ item }) => {
	return (
		<div className="flex flex-1 flex-row items-center justify-between rounded-16 bg-white p-24 shadow-md lg:p-16">
			<h4 className="font-regular text-16 text-black">{item?.title}</h4>
			<p className="text-16 font-bold" style={{ color: item.color }}>
				{item?.value} <span>{item?.unit}</span>
			</p>
		</div>
	);
};

export default CardItem;
