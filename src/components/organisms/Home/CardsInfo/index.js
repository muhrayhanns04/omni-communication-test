import CardItem from "./Item";
import React from "react";

const CardsInfo = ({ title, data }) => {
	return (
		<div className="mx-24 mt-32">
			<h2 className="mb-16 font-extra-bold text-24 text-black ">
				{title}
			</h2>
			<div className="grid gap-24 lg:grid-cols-6">
				{data?.map((item, index) => {
					return (
						<React.Fragment key={index}>
							<CardItem item={item} />
						</React.Fragment>
					);
				})}
			</div>
		</div>
	);
};

export default CardsInfo;
