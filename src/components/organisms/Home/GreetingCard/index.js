import React from "react";

const GreetingCard = ({ title, paragraph }) => {
	return (
		<div className="mx-24 rounded-16 border-t-8 border-t-red-500 bg-white p-24 drop-shadow-md">
			<h1 className="mb-8 text-24 font-bold text-black">{title}</h1>
			<p className="font-regular text-16 text-gray-300">{paragraph}</p>
		</div>
	);
};

export default GreetingCard;
