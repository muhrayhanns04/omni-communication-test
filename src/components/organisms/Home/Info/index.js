import { Number } from "../../../../helpers/Common";
import React from "react";

const Info = ({ value }) => {
	return (
		<h3 className="self-center text-center font-regular text-16 text-black">
			Moving average for last 10 second{" "}
			<span className="font-bold">{Number(value)}</span>
		</h3>
	);
};

export default Info;
