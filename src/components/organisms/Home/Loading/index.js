import React from "react";

const Loading = ({ value }) => {
	return (
		<div className="m-auto w-90%">
			<h3 className="self-center text-center font-regular text-16 text-black">
				wait a moment we are preparing your data ({value})...
			</h3>
		</div>
	);
};

export default Loading;
