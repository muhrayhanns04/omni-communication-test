import { IoCaretDownOutline, IoCaretUpOutline } from "react-icons/io5";

import { NavLink } from "react-router-dom";
import React from "react";

const SidebarItem = ({
	icon,
	title,
	linkTo,
	haveChild,
	onClick,
	isOpen,
	isChilds,
}) => {
	return (
		<button
			className="relative w-full border-none outline-none"
			onClick={onClick}
		>
			<NavLink
				to={isChilds ? "/home" : linkTo}
				className="flex w-full flex-row items-center justify-between"
			>
				<section className="flex flex-row items-center">
					{icon}
					<span className="ml-16 font-regular text-16 text-white">
						{title}
					</span>
				</section>
				{haveChild && isOpen ? (
					<IoCaretUpOutline fill="#FFFFFF" />
				) : haveChild && !isOpen ? (
					<IoCaretDownOutline fill="#FFFFFF" />
				) : null}
			</NavLink>
		</button>
	);
};

export default SidebarItem;
