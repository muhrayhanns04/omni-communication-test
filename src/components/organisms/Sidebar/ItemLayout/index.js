import React from "react";
import SidebarItem from "../../Navbar/Item";
import { toggleButton } from "../../../../features/navbarSlice";
import { useDispatch } from "react-redux";

const ItemLayout = ({ item }) => {
	const [isOpen, setIsOpen] = React.useState(true);
	const dispatch = useDispatch();

	return (
		<div onClick={() => dispatch(toggleButton())}>
			<SidebarItem
				title={item.title}
				linkTo={item.link}
				icon={item.icon}
				haveChild={item.haveChild}
				isOpen={isOpen}
				isChilds={item?.childs}
				onClick={() => setIsOpen(!isOpen)}
			/>
			{isOpen && (
				<div className="ml-8 mt-16 flex flex-col">
					{item?.childs?.map((childItem, childIndex) => (
						<div
							className="mb-16 flex h-32 flex-row items-center border-l-2 border-l-white py-24"
							key={childIndex}
						>
							<SidebarItem
								title={childItem.title}
								linkTo={childItem.link}
							/>
						</div>
					))}
				</div>
			)}
		</div>
	);
};

export default ItemLayout;
