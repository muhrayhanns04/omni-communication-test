import React from "react";

const TableHeadItem = ({ item }) => {
	return (
		<th title={item} className="bg-gray-30 py-[12px] px-24 text-left">
			{item.toUpperCase()}
		</th>
	);
};

export default TableHeadItem;
