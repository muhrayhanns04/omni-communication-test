import { FaSortAmountUpAlt } from "react-icons/fa";
import { IoFunnelSharp } from "react-icons/io5";
import React from "react";

const TableHeader = ({ title }) => {
	return (
		<div className="flex flex-row items-center justify-between px-24">
			<h2 className="mb-16 font-semi-bold text-16 text-red-500">
				{title}
			</h2>
			<div className="flex flex-row items-center space-x-16">
				<button className="flex flex-row items-center">
					<IoFunnelSharp className="mr-16" />
					<p className="font-regular text-16 text-gray-700">Sort</p>
				</button>
				<button className="flex flex-row items-center">
					<FaSortAmountUpAlt className="mr-16" />
					<p className="font-regular text-16 text-gray-700">Filter</p>
				</button>
			</div>
		</div>
	);
};

export default TableHeader;
