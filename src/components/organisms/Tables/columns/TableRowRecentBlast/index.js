import React from "react";

const TableRowRecentBlast = ({ data }) => {
	return (
		<tr className="odd:bg-white even:bg-slate-50">
			{data.map((item, index) => (
				<td
					key={index}
					className={`py-16 px-24 text-left ${
						index == 0 && "font-bold text-gray-700"
					}`}
				>
					{item}
				</td>
			))}
		</tr>
	);
};

export default TableRowRecentBlast;
