import React from "react";
import TableHeadItem from "./TableHeadItem";
import TableHeader from "./TableHeader";
import TableRowRecentBlast from "./columns/TableRowRecentBlast";

const Tables = ({ rows, headers, title }) => {
	return (
		<div className="relative mx-24 mt-32 overflow-x-auto shadow-md sm:rounded-lg">
			<TableHeader title={title} />
			<table className="relative h-[12px] w-full table-auto overflow-scroll lg:table-fixed">
				<thead className="text-16 font-bold text-gray-700">
					<tr>
						{headers.map((item, index) => (
							<TableHeadItem item={item} key={index} />
						))}
					</tr>
				</thead>
				<tbody className="divide-y divide-slate-100 overflow-y-scroll font-regular text-16 text-gray-300">
					{rows.map((item, index) => (
						<TableRowRecentBlast data={item.items} key={index} />
					))}
				</tbody>
			</table>
		</div>
	);
};

export default Tables;
