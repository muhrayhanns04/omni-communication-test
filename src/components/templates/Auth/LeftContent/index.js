import ButtonComponent from "../../../atoms/ButtonComponent";
import InputHook from "../../../atoms/InputHook";
import LOGO from "../../../../assets/icons/logo.svg";
import React from "react";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";

const LeftContent = () => {
	let navigate = useNavigate();
	const { register, handleSubmit } = useForm();

	const onSubmit = (data) => {
		localStorage.setItem(
			"@token",
			"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
		);
		navigate("/home");
	};

	return (
		<div className="flex h-screen w-50% flex-col items-center justify-center bg-white">
			<img
				src={LOGO}
				alt="PT Omni Communication Assistance"
				className="mb-16"
			/>
			<p className="w-[45%] text-center font-regular text-16 text-gray-300">
				Welcome Back, Please login into your account
			</p>
			<form
				className="mdx:w-[90%] mt-32 md:w-[90%] 2xl:w-[45%]"
				onSubmit={handleSubmit(onSubmit)}
			>
				<InputHook
					register={register}
					label="Username / Email"
					name="username"
					type="text"
					autocomplete="on"
					inputClassName="input-large-background"
				/>
				<InputHook
					register={register}
					label="Password"
					name="password"
					type="password"
					className="mb-32"
					inputClassName="input-large-background"
					autocomplete="current-password"
				/>
				<ButtonComponent value="Login" />
			</form>
		</div>
	);
};

export default LeftContent;
