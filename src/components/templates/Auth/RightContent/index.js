import React from "react";
import THUMB from "../../../../assets/images/thumb.png";

const RightContent = () => {
	return (
		<div className="flex h-screen w-50% flex-col items-center justify-center bg-red-500">
			<img src={THUMB} alt="thumb" className="mb-64" />
		</div>
	);
};

export default RightContent;
