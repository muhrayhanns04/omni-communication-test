import Header from "../../organisms/Header";
import React from "react";
import Sidebar from "../../atoms/Sidebar";

const Layout = ({ children }) => {
	return (
		<div className="flex flex-row">
			<Sidebar />
			<main className="w-full">
				<Header />
				{children}
			</main>
		</div>
	);
};

export default Layout;
