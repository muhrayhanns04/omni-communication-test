import CardsInfo from "../../components/organisms/Home/CardsInfo";
import GreetingCard from "../../components/organisms/Home/GreetingCard";
import React from "react";
import Tables from "../../components/organisms/Tables";
import data from "../../api/cardsInfo.json";
import rows from "../../api/tables.json";

const headers = [
	"Id Name",
	"Type",
	"Campaign",
	"Total Blast",
	"Status",
	"Date",
];

const HomePage = () => {
	const parag = (
		<span>
			Let’s start make some noise and make your campaign great again!{" "}
			<button className="font-semi-bold text-red-500">click here</button>{" "}
			to spread your messages.
		</span>
	);

	return (
		<div>
			<GreetingCard title="Hello, Welcome to OCA!" paragraph={parag} />
			<CardsInfo data={data} title="Remaining Quota" />
			<Tables title="RECENT BLAST" headers={headers} rows={rows} />
			<Tables title="RECENT REGISTERED" headers={headers} rows={rows} />
			<Tables title="RECENT PRODUCT" headers={headers} rows={rows} />
		</div>
	);
};

export default HomePage;
