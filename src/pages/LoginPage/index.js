import LeftContent from "../../components/templates/Auth/LeftContent";
import React from "react";
import RightContent from "../../components/templates/Auth/RightContent";

const LoginPage = () => {
	return (
		<div className="relative flex h-screen flex-row items-center">
			<LeftContent />
			<RightContent />
		</div>
	);
};

export default LoginPage;
